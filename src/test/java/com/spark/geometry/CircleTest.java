package com.spark.geometry;

import org.junit.Test;
import static org.junit.Assert.*;

public class CircleTest {

    @Test
    public void area() {
	Circle circle = new Circle();

	circle.setRadius(0);
	assertEquals(0, circle.area(), 0.001);

	circle.setRadius(95);
	assertEquals(28352.8737, circle.area(), 0.001);

	circle.setRadius(42.52);
	assertEquals(5679.84369, circle.area(), 0.001);
    }

    @Test
    public void perimeter() {
	Circle circle = new Circle();

	circle.setRadius(0);
	assertEquals(0, circle.perimeter(), 0.001);

	circle.setRadius(78.25);
	assertEquals(491.65925, circle.perimeter(), 0.001);

	circle.setRadius(153.45);
	assertEquals(964.15478, circle.perimeter(), 0.001);
    }
}
