package com.spark.geometry;

import org.junit.Test;
import static org.junit.Assert.*;

public class TriangleTest {

    @Test
    public void area() {
	Triangle triangle = new Triangle();

	triangle.setBase(0);
	triangle.setHeight(0);
	assertEquals(0, triangle.area(), 0.001);

	triangle.setBase(25);
	triangle.setHeight(74);
	assertEquals(925, triangle.area(), 0.001);

	triangle.setBase(15.32);
	triangle.setHeight(54.28);
	assertEquals(415.7848, triangle.area(), 0.001);
    }

    @Test
    public void perimeter() {
	Triangle triangle = new Triangle();

	triangle.setBase(0);
	triangle.setHeight(0);
	assertEquals(0, triangle.perimeter(), 0.001);

	triangle.setBase(78);
	triangle.setHeight(91);
	assertEquals(288.854, triangle.perimeter(), 0.001);

	triangle.setBase(45.25);
	triangle.setHeight(63.47);
	assertEquals(186.668, triangle.perimeter(), 0.001);
    }
}
