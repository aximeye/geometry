package com.spark.geometry;

import org.junit.Test;
import static org.junit.Assert.*;

public class RectangleTest {

    @Test
    public void testArea() {
	String errorMessage = "Error calculating area!!!";
	Rectangle rectangle = new Rectangle();

	rectangle.setLength(0);
	rectangle.setBreadth(0);
	assertEquals(0, rectangle.area(), 0.001);

	rectangle.setLength(2);
	rectangle.setBreadth(2);
	assertEquals(4, rectangle.area(), 0.001);

	rectangle.setLength(3.4);
	rectangle.setBreadth(5.6);
	assertEquals(19.04, rectangle.area(), 0.001);
    }
    
    @Test
    public void testPerimeter() {
	String errorMessage = "Error calculating perimeter!!!";
	Rectangle rectangle = new Rectangle();

	rectangle.setLength(0);
	rectangle.setBreadth(0);
	assertEquals(0, rectangle.perimeter(), 0.001);

	rectangle.setLength(2);
	rectangle.setBreadth(3);
	assertEquals(10, rectangle.perimeter(), 0.001);

	rectangle.setLength(2.3);
	rectangle.setBreadth(4.5);
	assertEquals(13.6, rectangle.perimeter(), 0.001);
    }
}
