package com.spark.geometry;

 /**
 * Class that calculates area and circumference of Circle.
 * 
 * @author  Ashish Bhagat
 * @version 1.0
 */
public class Circle implements Product {
    private double radius;

    public static final double PI = Math.PI;

	/**
	 * Returns radius of Circle.
	 * @return double Radius of Circle.
	 */
    public double getRadius() {
		return this.radius;
    }

	/**
	 * Initializes radius of Circle to value passed by caller.
	 * @param radius Radius of Circle.
	 */
    public void setRadius(double radius) {
		this.radius = radius;
    }

	/**
	 * Returns area of Circle.
	 * @return double Area of Circle.
	 */
    @Override
    public double area() {
		return PI * (radius * radius);
    }

	/**
	 * Returns Perimeter of Circle.
	 * @return double Perimeter of Circle.
	 */
    @Override
    public double perimeter() {
		return 2 * PI * radius;
    }
}
