package com.spark.geometry;

import java.util.Scanner;
import java.util.logging.Logger;
import java.util.logging.Level;

 /**
 * Menu driven program to calculate area and perimeter of 
 * Rectangle, Triangle, and Circle
 * @author  Ashish Bhagat
 * @version 1.0
 */
public class App 
{
    private static final Logger logger = Logger.getLogger( App.class.getName() );

    public static void main( String[] args ) {
		App app = new App();
		
		do {
			app.displayMenu();
			int userOption = app.getUserMenuOption();
			
			switch (userOption) {
				case 1:
					app.calculateAreaAndPerimeterOfRectangle();
					break;
				case 2:
					app.calculateAreaAndPerimeterOfCircle();
					break;
				case 3:
					app.calculateAreaAndPerimeterOfTriangle();
					break;
				case 4:
					System.exit(0);
			}
		} while (true);
    }

	/**
	 * Displays menu containing types of geometric figures whose area and perimeter
	 * can be calculated.
	 */
	public void displayMenu() {
		logger.log(Level.INFO, "Calculate area and perimeter of:");
		logger.log(Level.INFO, "1. Rectangle");
		logger.log(Level.INFO, "2. Circle");
		logger.log(Level.INFO, "3. Triangle");
		logger.log(Level.INFO, "4. Option: ");
	}
	
	/**
	 * Accepts input from user.
	 * @return int
	 */
	public int getUserMenuOption() {
		Scanner reader = new Scanner(System.in);
		int userOption = 0;
		
		while (!reader.hasNextInt()) {
			reader.next();
		}
		userOption = reader.nextInt();
		
		return userOption;
	}
	
	/**
	 * Calculates area and perimeter of Rectangle
	 */
	public void calculateAreaAndPerimeterOfRectangle() {
		Scanner reader = new Scanner(System.in);
		Rectangle rectangle  = new Rectangle();
		double length = 0;
		double breadth = 0;
		
		logger.log(Level.INFO, "Enter length: ");
		length = reader.nextDouble();
		rectangle.setLength(length);
		
		logger.log(Level.INFO, "Enter breadth: ");
		breadth = reader.nextDouble();
		rectangle.setBreadth(breadth);
		
		displayAreaAndPerimeterOfProduct(rectangle);
	}
	
	/**
	 * Calculates area and perimeter of Triangle
	 */
	public void calculateAreaAndPerimeterOfTriangle() {
		Scanner reader = new Scanner(System.in);
		Triangle triangle = new Triangle();
		double base = 0;
		double height = 0;
		
		logger.log(Level.INFO, "Enter base: ");
		base = reader.nextDouble();
		triangle.setBase(base);
		
		logger.log(Level.INFO, "Enter height: ");
		height = reader.nextDouble();
		triangle.setHeight(height);
		
		displayAreaAndPerimeterOfProduct(triangle);
	}

	/**
	 * Calculates area and perimeter of Circle
	 */
	public void calculateAreaAndPerimeterOfCircle() {
		Scanner reader = new Scanner(System.in);
		Circle circle = new Circle();
		double radius = 0;
		
		logger.log(Level.INFO, "Enter radius: ");
		radius = reader.nextDouble();
		circle.setRadius(radius);
		
		displayAreaAndPerimeterOfProduct(circle);
	}
	
	/**
	 * Displays area and perimeter of Triangle on the console.
	 * @param product Type of product.
	 */
	public void displayAreaAndPerimeterOfProduct(Product product) {
		logger.log(Level.INFO, "------------Area and perimeter of Product:");
		logger.log(Level.INFO, "------------Area: " + product.area());
		logger.log(Level.INFO, "------------Perimeter: " + product.perimeter());
	}
}
