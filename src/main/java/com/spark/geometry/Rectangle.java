package com.spark.geometry;

 /**
 * Class that calculates area and perimeter of Rectangle.
 * 
 * @author  Ashish Bhagat
 * @version 1.0
 */
public class Rectangle implements Product {
    private double length;
    private double breadth;

    public double getLength() {
		return this.length;
    }

    public void setLength(double length) {
		this.length = length;
    }

    public double getBreadth() { 
		return this.breadth;
    }

    public void setBreadth(double breadth) {
		this.breadth = breadth;
    }
    
    @Override
    public double area() {
		return length * breadth;
    }
    
    @Override
    public double perimeter() {
		return (2*length) + (2*breadth);
    }
}
