package com.spark.geometry;

public interface Product {
    public double area();

    public double perimeter();
}
