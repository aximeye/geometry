package com.spark.geometry;

 /**
 * Class that calculates area and perimeter of Triangle.
 * 
 * @author  Ashish Bhagat
 * @version 1.0
 */
public class Triangle implements Product {
    private double base;
    private double height;

	/**
	 * Return base of Triangle.
	 * @return double Base of triangle
	 */
    public double getBase() {
		return base;
    }

	/**
	 * Initializes base of Triangle.
	 * @param base Base of triangle passed by caller.
	 */
    public void setBase(double base) {
		this.base = base;
    }

	/**
	 * Return height of triangle.
	 * @return double Height of triangle.
	 */
    public double getHeight() {
		return height;
    }

	/**
	 * Initializes height of triangle.
	 * @param height Height to Triangle passed by caller.
	 */
    public void setHeight(double height) {
		this.height = height;
    }

	/**
	 * Retuns area of Triangle.
	 * @return double Area of Triangle.
	 */
    @Override
    public double area() {
		return .5 * base * height;
    }

	/**
	 * Return perimeter of Triangle
	 * @return double Perimeter of Triangle
	 */
    @Override
    public double perimeter() {
		double hypotenuse = (base*base) + (height*height);

		return base + height + Math.sqrt(hypotenuse);
    }
}
